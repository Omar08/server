const express = require('express');
const app = express();
const bodyParser = require('body-Parser');
const morgan = require('morgan');
const logger = require('winston');
const {moongose} = require('./db');

// Agregamos el modelo
var Autor = require('./models/autor')


app.use(bodyParser.json());
app.use(morgan('common'))



const port = 3000;
var router = express.Router();

router.get('/',(req,res)=>{
    logger.info("Home page");
    res.json({"message":"Welcome to API :)"});
});



router.route('/autor').post((req,res)=>{
    logger.info('Create Autor')
    var autor = new Autor();

    autor.nombre = req.body.nombre;
    autor.apellido = req.body.apellido;
    autor.edad = req.body.edad;

    autor.save((err)=>{
        if(err){
            res.send("El error es",err)
        }else{
            res.json({"message":"Autor creado"})
        }
    });
});

router.route('/getAutors').get((req,res)=>{
    logger.info('Obteniendo a los Autores')
    Autor.find((err,autor)=>{
        if(err){
            res.send(err);
        }else{
            res.json(autor)
        }
    })
});

router.route('/getAutorsById/:autor_id').get((req,res)=>{
    logger.info('Obteniendo a un Autor por ID')
    
    logger.info('Obteniendo a los Autores')
    Autor.findById( req.params.autor_id,(err,autor)=>{
        if(err){
            res.send(err);
        }else{
            res.json(autor)
        }
    })
});


router.route('/updateAutorById/:autor_id').put((req,res)=>{

    logger.info("Actualizacion de Autores");
    Autor.findById(req.params.autor_id,(err,autor)=>{
        if(err){
            res.send(err);
        }else{
            autor.nombre = req.body.nombre;
            autor.apellido = req.body.apellido;
            autor.edad = req.body.edad;

            autor.save((err)=>{
                if(err){
                    res.send(err);
                }else{
                    res.json({"message":"Autor actualizado !!"})
                }
            })
        }
    })
})


router.route('/deleteAutorById/:autor_id').delete((req,res)=>{

    Autor.remove({_id: req.params.autor_id},(err,autor)=>{
        if(err){
            res.send(err)
        }else{
            res.json({'message':'Autor eliminado correctamente'})
        }
    })
   
})
//Route

app.use('/api',router);
app.listen(port);
console.log("Servidor corriendo en el puerto ", port)

