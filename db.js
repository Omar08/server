const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/dbtest',{
    useNewUrlParser:true
}).then(()=>{
    console.log("Conexion exitosa a MONGODB")
}).catch((err)=>{
    console.log("Upss hubo un error al intentar conectarse ", err)
})

module.exports = mongoose;