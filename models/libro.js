var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var libroSchema = new Schema({
    nombre:String,
    descripcion: string,
    num_pagina:string,
    idAutor:{type:Schema.ObjectId, ref:'autor'}
});


module.exports = mongoose.model('libro',libroSchema)