var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var autorSchema = new Schema({
    nombre:String,
    apellido:String,
    edad:Number
}); 


module.exports = mongoose.model('Autor',autorSchema);